# CONDUCTÍMETRO

Herramienta libre para la determinación de conductividad (μS/cm) en soluciones acuosas.

Adaptación del codigo de DFRobot Gravity: Analog TDS Sensor/Meter
https://www.dfrobot.com/wiki/index.php/Gravity:_Analog_TDS_Sensor_/_Meter_For_Arduino_SKU:_SEN0244

Librerias control sensor temperatura: ds18b20
https://github.com/PaulStoffregen/OneWire
https://github.com/milesburton/Arduino-Temperature-Control-Library

![foto](/Imagenes/conductimetro2.jpg)

![foto](/Imagenes/conductimetro1.jpg)

![foto](Imagenes/Screenshot.png)
